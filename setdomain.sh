#!/bin/bash

#!/bin/bash

NEW_HOST=${HOSTNAME%%.*}

TEMPLATE='
server {
  server_name  .#HOSTNAME#;
  listen       80;
  charset utf-8;
  sendfile off;
  set $domain $host;

  if ($domain ~ "^(.*)\.(.*)$") {
   set $domain $1;
   set $userTLD $2;
   set $servername "${domain}.docker";
  }
   # location /test {return 200 "$domain // $userTLD // $servername";}

  location / {
    proxy_set_header host $servername;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    sub_filter_types text/css;
    sub_filter_once off;
    sub_filter $servername $host;

    proxy_pass http://127.0.0.1;
  }
}
'

result_string="${TEMPLATE//#HOSTNAME#/$NEW_HOST}"

echo "$result_string" > ~/dockerSites/conf/nginx/$NEW_HOST.conf