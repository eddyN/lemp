# Docker LEMP

This docker is used as a generic development environment for all PHP/MySQL projects.

## Install
To make things easy to find we install the lemp in ~/dockerSites
```
mkdir -p ~/dockerSites
cd ~/dockerSites
git clone git@gitlab.com:eddyN/lemp.git
cd lemp
docker build .
docker-compose up -d
```

## Dependencies

### Docker for mac
https://docs.docker.com/engine/installation/mac/#/docker-for-mac


### Dnsmasq

Add *.docker to resolver

```
sudo mkdir -p /etc/resolver

echo "nameserver 0.0.0.0" | sudo tee /etc/resolver/docker > /dev/null
echo "port 53535" | sudo tee -a /etc/resolver/docker > /dev/null
```

## Usage

go to [db](http://db.docker/)

User: root
Pass: river


go to [logs](http://logs.docker/)


## Location

place code in ~/dockerSites/www/
control lemp from  ~/dockerSites/lemp/

example:

folder `~/dockerSites/www/mypage/public` will become `http://mypage.docker`

folder `~/dockerSites/www/mypage.TLD/public` will become `http://mypage.TLD.docker`

folder `~/dockerSites/www/www.mypage.TLD/public` will become `http://www.mypage.TLD.docker`


## Update (not tested)
mkdir -p ~/dockerSites/lemp
docker-compose stop
docker pull registry.edport.se/docker/lemp:latest
docker-compose up -d
