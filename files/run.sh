#!/bin/bash

#Set mysql ROOT password
if [ ! -f /var/lib/mysql/mysql_configured ]; then
    echo "=> MySQL not configured yet, configuring MySQL ..."

    /etc/init.d/mysql start

    echo "=> Waiting till MySQL is started"
    mysqladmin --wait=30 ping > /dev/null 2>&1

    echo "=> Creating MySQL root user and granting external access";
    mysql -uroot -e "CREATE USER 'root'@'%' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD';"
    mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;"
    mysql -uroot -e "UPDATE mysql.user SET Password = PASSWORD('$MYSQL_ROOT_PASSWORD')WHERE User = 'root'; FLUSH PRIVILEGES;"

    touch /var/lib/mysql/mysql_configured

    /etc/init.d/mysql stop
else
    echo "=> MySQL is already configured"
fi

echo "nameserver 127.0.0.1" > /etc/resolv.conf

# Fix hostname for sendmail
# This is fixed in docker-compose hostname directive.
# /tmp/sendmail.sh

#Start services
exec /usr/local/bin/supervisord -n -c /etc/supervisord.conf
